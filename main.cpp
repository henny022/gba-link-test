#include <gba_console.h>
#include <gba_video.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>
#include <gba_input.h>
#include <gba_sio.h>
#include <fmt/format.h>

namespace console
{
void init()
{
    consoleDemoInit();
}

void clear()
{
    fmt::print("\033[2J");
}

void print(auto s)
{
    fmt::print("{}", s);
}

void print(int line, int col, auto s)
{
    fmt::print("\033[{};{}H{}", line, col, s);
}

void println(auto s)
{
    fmt::print("{}\n", s);
}

void println(int line, auto s)
{
    fmt::print("\033[{};0H{}\n", line, s);
}

template<typename... Ts>
void printf(fmt::format_string<Ts...> fmt, Ts &&...ts)
{
    fmt::print(fmt, std::forward<Ts>(ts)...);
}

template<typename... Ts>
void printf(int line, int col, fmt::format_string<Ts...> fmt, Ts &&...ts)
{
    print(line, col, fmt::format(fmt, std::forward<Ts>(ts)...));
}
}

enum struct LinkStatus
{
    Child,
    Parent,
};

LinkStatus select_loop()
{
    console::println("press A for parent");
    console::println("press B for child");
    while (true)
    {
        scanKeys();
        auto down = keysDown();
        if (down & KEY_A)
        {
            return LinkStatus::Parent;
        }
        if (down & KEY_B)
        {
            return LinkStatus::Child;
        }
        VBlankIntrWait();
    }
}

[[noreturn]]
void error_loop()
{
    console::clear();
    console::println("error linking up");
    while (true)
    {
        VBlankIntrWait();
    }
}

void wait_for_key(auto prompt, KEYPAD_BITS key)
{
    console::println(prompt);
    while (true)
    {
        scanKeys();
        if (keysDown() & key)
        {
            break;
        }
        VBlankIntrWait();
    }
}

void parent_loop()
{
    console::clear();
    console::println("parent");

    REG_SIODATA32 = 'p';

    REG_SIOCNT |= SIO_SO_HIGH;

    while (REG_SIOCNT & SIO_RDY)
    {
        VBlankIntrWait();
    }
    REG_SIOCNT |= SIO_START;
    while (REG_SIOCNT & SIO_START)
    {
        VBlankIntrWait();
    }
    REG_SIOCNT &= ~SIO_SO_HIGH;

    u32 d = REG_SIODATA32;
    if (d == 0xffffffff)
    {
        console::println("bad transfer");
    }
    else
    {
        console::printf("received {}\n", (char) d);
    }
    wait_for_key("press A to retry...", KEY_A);
}

void child_loop()
{
    console::clear();
    console::println("child");

    REG_SIODATA32 = 'c';

    REG_SIOCNT |= SIO_START;
    REG_SIOCNT &= ~SIO_SO_HIGH;
    while (REG_SIOCNT & SIO_START)
    {
        VBlankIntrWait();
    }
    REG_SIOCNT |= SIO_SO_HIGH;

    u32 d = REG_SIODATA32;
    if (d == 0xffffffff)
    {
        console::println("bad transfer");
    }
    else
    {
        console::printf("received {}\n", (char) d);
    }
    wait_for_key("press A to retry...", KEY_A);
}

[[noreturn]]
void AGBMain()
{
    irqInit();
    irqEnable(IRQ_VBLANK);

    console::init();

    console::println("v4");

    auto state = select_loop();
    switch (state)
    {
        case LinkStatus::Parent:
            REG_RCNT = R_NORMAL;
            REG_SIOCNT = SIO_32BIT;
            REG_SIOCNT |= SIO_CLK_INT;
            parent_loop();
            parent_loop();
            parent_loop();
            break;
        case LinkStatus::Child:
            REG_RCNT = R_NORMAL;
            REG_SIOCNT = SIO_32BIT;
            child_loop();
            child_loop();
            child_loop();
            break;
    }
    console::clear();
    console::println("done");
    while (true)
    {
        VBlankIntrWait();
    }
}

auto main() -> int
{
    AGBMain();
}
