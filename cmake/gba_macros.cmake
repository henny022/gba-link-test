if (NOT GBAFIX)
    find_program(GBAFIX gbafix ${DEVKITPRO}/tools/bin)
endif ()

macro(add_gba name target)
    if (NOT GBAFIX)
        message(FATAL_ERROR "gbafix not found")
    endif ()
    add_custom_command(
            OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${target}.gba
            COMMAND ${CMAKE_OBJCOPY} -O binary --gap-fill 0xff --pad-to 0x9000000 $<TARGET_FILE:${target}> ${CMAKE_CURRENT_BINARY_DIR}/${target}.gba
            COMMAND ${GBAFIX} ${target}.gba
            DEPENDS ${target}
            VERBATIM
    )
    add_custom_target(
            ${name} ALL
            SOURCES ${CMAKE_CURRENT_BINARY_DIR}/${target}.gba
    )
endmacro()
