set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR armv4t)

set(DEVKITPRO $ENV{DEVKITPRO})
if (NOT IS_DIRECTORY "${DEVKITPRO}")
    set(DEVKITPRO "/opt/devkitpro")
endif ()

set(DEVKITARM $ENV{DEVKITARM})
if (NOT IS_DIRECTORY "${DEVKITARM}")
    set(DEVKITARM "${DEVKITPRO}/devkitARM")
endif ()

if (NOT IS_DIRECTORY "${DEVKITPRO}" OR NOT IS_DIRECTORY "${DEVKITARM}")
    message(FATAL_ERROR "devkitpro not set up correctly")
endif ()

set(CMAKE_C_COMPILER "${DEVKITARM}/bin/arm-none-eabi-gcc")
set(CMAKE_CXX_COMPILER "${DEVKITARM}/bin/arm-none-eabi-g++")
set(CMAKE_AR "${DEVKITARM}/bin/arm-none-eabi-ar")
set(CMAKE_RANLIB "${DEVKITARM}/bin/arm-none-eabi-ranlib")
set(CMAKE_OBJCOPY "${DEVKITARM}/bin/arm-none-eabi-objcopy")

set(CMAKE_FIND_ROOT_PATH ${DEVKITARM} ${DEVKITPRO})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

add_definitions(-DARM7 -D__GBA__)

set(ARCH "-march=armv4t -mcpu=arm7tdmi -mtune=arm7tdmi -mfloat-abi=soft -mtp=soft")
set(CMAKE_C_FLAGS "${ARCH}")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}")
