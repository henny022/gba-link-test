if (NOT DEVKITPRO)
    message(FATAL_ERROR "libgba requires devkitpro")
endif ()

set(LIBGBA_PATHS $ENV{LIBGBA} libgba ${DEVKITPRO}/libgba)

find_path(
        LIBGBA_INCLUDE_DIR gba.h
        PATHS ${LIBGBA_PATHS}
        PATH_SUFFIXES include libgba/include
)

find_library(
        LIBGBA_LIBRARY
        NAMES gba libgba.a
        PATHS ${LIBGBA_PATHS}
        PATH_SUFFIXES lib libgba/lib
)

set(LIBGBA_LIBRARIES ${LIBGBA_LIBRARIY})
set(LIBGBA_INCLUDE_DIRS ${LIBGBA_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
        LIBGBA
        DEFAULT_MSG
        LIBGBA_LIBRARY
        LIBGBA_INCLUDE_DIR
)
mark_as_advanced(LIBGBA_INCLUDE_DIR LIBGBA_LIBRARY)
if (LIBGBA_FOUND)
    add_library(libgba::libgba STATIC IMPORTED GLOBAL)
    set_target_properties(libgba::libgba PROPERTIES IMPORTED_LOCATION "${LIBGBA_LIBRARY}")
    target_include_directories(libgba::libgba INTERFACE ${LIBGBA_INCLUDE_DIR})
endif ()

